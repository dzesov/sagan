#!/usr/bin/env sh

java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Xmx256M -jar /apps/app.jar --server.port=$PORT